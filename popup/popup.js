/**
 *
 * @author Tatevosyan Artem (@teqst)
 */



import vkQr from '../js/libs/vkqr.js';
const manifest = chrome.runtime.getManifest();
const reader = new FileReader();

const title = document.getElementById('popup__title');
const settingsBtn = document.querySelector('.popup__header-right');
const donateBtn = document.querySelector('.popup__header-left');
const qrCanvas = document.querySelector('.popup__qrcode-canvas');
const notificationError = document.getElementById('notification-error');
const customURI = document.getElementById('url');

const qrShowBackground = document.getElementById('show-background'),
    qrShowLogo = document.getElementById('show-logo'),
    qrBackColor = document.getElementById('back-color'),
    qrForeColor = document.getElementById('fore-color'),
    qrDownloadPng = document.getElementById('download-png'),
    qrLogoData = document.getElementById('logo-data');


title.textContent = `${manifest.name} (${manifest.version})`;

generateVkQr();

//Event listeners

/**
 * @Description: Settings button listener
 *
 */
settingsBtn.addEventListener('click', () => {
    notificationError.classList.add('active');
    notificationError.querySelector('.notification__text').textContent = 'Sorry, settings page still under development';
    setTimeout(() => {
        notificationError.classList.remove('active');
    }, 4000);
});

qrLogoData.addEventListener('change', () => {
    const file = document.querySelector('input[type=file]').files[0];
    reader.addEventListener('load', generateVkQr, false);

    if (file) reader.readAsDataURL(file);
});

/**
 * @Description: Rerender QR on changing parameters
 *
 */
[
    qrShowLogo,
    qrShowBackground,
    qrBackColor,
    qrForeColor,
    customURI
].forEach(qrParam => {
    qrParam.addEventListener('input', generateVkQr);
})

/**
 * @Description: Redirect to PayPal account
 *
 */
donateBtn.addEventListener('click', chrome.tabs.create({url: 'https://paypal.me/erkemeytadevonts'}));

/**
 * @Description: Download png image on click
 *
 */
qrDownloadPng.addEventListener('click', qrToImage);


//Functions

async function generateVkQr() {
    const pageQuery = {
        url: new URL(window.location.href).searchParams.get('url_to_encode')
    };

    const textData = customURI.value ? { url: customURI.value } : pageQuery.url !== null ? pageQuery : await getCurrentTab();
    let eccLevel = 3;

    if (textData.url.length > 250) {
        return qrCanvas.innerHTML = `<div class="error">${chrome.i18n.getMessage('verylong')}</div>`;
    } else if (textData.url.length > 150) {
        eccLevel = 0;
    }

    const qrSVG = vkQr.createQR(textData.url, {
        logoData: reader.result || undefined,
        isShowLogo: qrShowLogo.checked,
        isShowBackground: qrShowBackground.checked,
        qrSize: 280,
        backgroundColor: qrBackColor.value,
        foregroundColor: qrForeColor.value,
        ecc: eccLevel
    })

    qrCanvas.innerHTML = qrSVG

    return [qrSVG, textData];
}

/**
 * @Description: Converts SVG QR Code to PNG image
 *
 */

async function qrToImage() {
    let [svg, uriString] = await generateVkQr();
    const canvas = document.getElementById('popcanvas');
    const ctx = canvas.getContext('2d');
    const domUrl = window.URL || window.webkitURL || window;
    const image = new Image();

    canvas.width = 300;
    canvas.height = 300;

    const blob = new Blob([svg], { type: 'image/svg+xml' });
    const url = domUrl.createObjectURL(blob);

    image.onload = function() {
        ctx.drawImage(image, 0, 0);
        domUrl.revokeObjectURL(url);

        const uri = canvas.toDataURL('image/png').replace('image/png', 'octet/stream');
        const downloadLink = document.createElement('a');
        downloadLink.href = uri;
        downloadLink.download = 'AVOQR_' + uriString.url.replace(/\D{4,5}:\/\//gmi, '') + '.png'
        downloadLink.click();
        window.URL.revokeObjectURL(uri);
    };

    image.src = url;

}

async function getCurrentTab() {
    let queryOptions = { active: true, currentWindow: true };
    let [tab] = await chrome.tabs.query(queryOptions);
    return tab;
}