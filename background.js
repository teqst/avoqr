/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

/**
 * @Description: Activated once if extension installed
 *
 */
chrome.runtime.onInstalled.addListener(async () => {
    let url = chrome.runtime.getURL("welcome/welcome.html");
    await chrome.tabs.create({ url });
    initContextMenuItem();
});

/**
 * @Description: Context menu functionality listener
 *
 */
chrome.contextMenus.onClicked.addListener((info) => {
    if (info.menuItemId === "AvoQRContext") {
        chrome.tabs.create({ url: 'popup/popup.html?url_to_encode=' + info.pageUrl });
    }
});

/**
 * @Description: Init Google Chrome context menu item for creating quick qr codes
 *
 */
function initContextMenuItem() {
    chrome.contextMenus.create({
        id: "AvoQRContext",
        title: "Create AvoQR for this page",
        contexts: ["all"]
    });
}
